package vn.com.javadev.springsercurity.java.configs;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;


@Configuration
@EnableAuthorizationServer
public class OAuth2Config extends AuthorizationServerConfigurerAdapter{
	 private final String CLIENT_ID = "clientapp";
	  private final String CLIENT_SECRET = "123456";
	  private final String PRIMARY_KEY =
	      "-----BEGIN RSA PRIVATE KEY-----\r\n" + "MIIEowIBAAKCAQEAuK8soGt2ZMZsld1HmmOgFm0z6BnzMugFpKYrnGK2VQyMxLQi\r\n" +
	          "M7nXwVuIH1fsHXHCO7z6JNemM8nAB6Kwbr+ThxkV6lFThZrdn/oPqPnQsECUb/Hy\r\n" +
	          "Wdv45MJN2kw3SStQN0HpdXK/eqMZrpYmo/qKZtzKAGkW/a4dz4XXlQ0EwDURs0bN\r\n" +
	          "1Thuw9roGrZqNUU0vt+K+fRHh+s8KG7bOUKnaIEteXiuZ5ZS5RJ/15WgjZJ0RyOd\r\n" +
	          "AMrDAekWmPBPCMM8bxdZrAvSIcGhiiOTCBZdCsGyb+3EucqcmSW1r0WulowQmVt3\r\n" +
	          "gXPNo+G2kw9cV+D+gfUjB/haYDiqzObn5qbmWwIDAQABAoIBAGKPYfKOxxVK66Ij\r\n" +
	          "AFZ9em7dgokIaEivmGthHv8LkyDaXLmRJHuUX3daQM/xXRntnunAlYJZ7+HNdoTc\r\n" +
	          "punDpZjAo3h0RG2V01bP3dGg2GVnYZBIf7zC7xYuN9wbNpn+2oVS0KOURGnPZyJM\r\n" +
	          "EX0LdRcmF0uWyeVCKbiZRg7Obs2f3IypVgnWoY9fKrcmMOHvXKew9azR9dI3YuJd\r\n" +
	          "GVJWauRJQaVZv/vrWQq0D94nYEw1pSTNhXJ/cH6WHrpWwMwZ9HgWQjMkPvHfDMy6\r\n" +
	          "u4GYhNG62OqQ5/+1fYxUmOEm9aLag14VfIJ9GguwibCoGl3pMEwB/NLIOoVeYHGO\r\n" +
	          "JtDswWECgYEA4CkTq+IKmXOaI8YDUMcqbYDV2JWdPrmSpC+C2g1tOVEKF577lAKE\r\n" +
	          "ChHnI8TUaKiEkAVj64Jg0ZWsMntlr1eeAnGIADENF0JpYO3il238ieNFXYuLCrCr\r\n" +
	          "rrwbTxHTB/5LLVvpHR44tfw05WOHheM4FelE2lSVPAfuzXVhsJsSX6sCgYEA0uqq\r\n" +
	          "GU/F92hjLsEYUDc6eYuZKBlCUqL9trz0B3Dbz5yZRnrZWTaazu7BgCvo/yCsEFEo\r\n" +
	          "qSChNnez+i4s3B2ZuEB0ej1aqZU4VZq2fRMOP+ffzJArKt5mipj19LXpLVztfqxA\r\n" +
	          "JOlTwVhEyhd1T9XJIodpl+ZJZUJf6ZDi6Q5vpBECgYB00pOQ8bYKcSf02t6he5rO\r\n" +
	          "BOXuAVM4/GfDL4unbnXs+CIlW1LLuV/tC5WFm7ADd0fW8yg29fTVZYafrJW7AxOz\r\n" +
	          "PE3fo7Cp3Y+j13ZO6yBBEHP3I6FsCdT/tMMauzRbJxkw44SjCzwNBArhPbTEfz2c\r\n" +
	          "lbyveINqDMxUwYcPDlJODwKBgQCmHJkw7CoPC5CpvvxGGOkd1vsdoJ8idCn75N8Q\r\n" +
	          "iSslXME+x/wsDhxog5O0PWD4piO9ih3K7d7qBeaiUAGWrl1lxqW1rGKeEt7WVM4K\r\n" +
	          "vZpIbHdPJfBJh/UcjRGQ1EA8MOb/D+xI5dbH/JvYyhbjdI0/e4SHVKBTdq4RRsHf\r\n" +
	          "KTZ18QKBgBu7JVBgSRb5bZXUf1WdMNqti3t0znJ8fQKmM6a9K7oRg9eZ/yyLiGdF\r\n" +
	          "3J9CYpX2FiohIQE23QwE6LhI/k9imaA4yTGWh5UtAfOoL/Tc0Wzv8NWwn7C2uUyj\r\n" +
	          "Cxrwyx68J8BybmAHaT86NYfz053QqG+nAVNbe/smHeOJi/6jjmRC\r\n" + "-----END RSA PRIVATE KEY-----";
	  private final String PUBLIC_KEY =
	      "-----BEGIN PUBLIC KEY-----\r\n" + "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuK8soGt2ZMZsld1HmmOg\r\n" +
	          "Fm0z6BnzMugFpKYrnGK2VQyMxLQiM7nXwVuIH1fsHXHCO7z6JNemM8nAB6Kwbr+T\r\n" +
	          "hxkV6lFThZrdn/oPqPnQsECUb/HyWdv45MJN2kw3SStQN0HpdXK/eqMZrpYmo/qK\r\n" +
	          "ZtzKAGkW/a4dz4XXlQ0EwDURs0bN1Thuw9roGrZqNUU0vt+K+fRHh+s8KG7bOUKn\r\n" +
	          "aIEteXiuZ5ZS5RJ/15WgjZJ0RyOdAMrDAekWmPBPCMM8bxdZrAvSIcGhiiOTCBZd\r\n" +
	          "CsGyb+3EucqcmSW1r0WulowQmVt3gXPNo+G2kw9cV+D+gfUjB/haYDiqzObn5qbm\r\n" + "WwIDAQAB\r\n" +
	          "-----END PUBLIC KEY-----";

	@Autowired
    private BCryptPasswordEncoder passwordEncoder;
	@Autowired
	DataSource dataSource;
	
	@Autowired
	@Qualifier("authenticationManagerBean")
	public AuthenticationManager authenticationManager;
	
	@Bean
	public TokenStore tokenStore() {
	   return new JdbcTokenStore(this.dataSource);
	}
	
	@Bean
	  public JwtAccessTokenConverter tokenEnhancer() {
	    JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
	    converter.setSigningKey(this.PRIMARY_KEY);
	    converter.setVerifierKey(this.PUBLIC_KEY);
	    return converter;
	  }

	@Override
    public void configure(final AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
      endpoints.authenticationManager(this.authenticationManager).tokenStore(tokenStore())
          .accessTokenConverter(tokenEnhancer());
    }
	
	
	 
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security
            .tokenKeyAccess("permitAll()")
            .checkTokenAccess("isAuthenticated()")
            .allowFormAuthenticationForClients();
    }
    
    

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
    	//System.out.println(passwordEncoder.encode("123456"));
        clients
            .inMemory()
            .withClient("clientapp").secret(passwordEncoder.encode("123456"))
            .authorizedGrantTypes("password", "authorization_code", "refresh_token")
            //.authorities("READ_ONLY_CLIENT")
            .autoApprove(true)
            .scopes("read","write")
           // .resourceIds("oauth2-resource")
           .redirectUris("http://localhost:8080/login/oauth2")
           // .redirectUriTemplate("{baseUrl}/login/oauth2/code/{registrationId}")
            .accessTokenValiditySeconds(1200)
            .refreshTokenValiditySeconds(240000);
    }
}
