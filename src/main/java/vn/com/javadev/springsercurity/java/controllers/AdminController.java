package vn.com.javadev.springsercurity.java.controllers;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/admin")
@PreAuthorize("isAuthenticated() and ((hasRole('USER')) or (hasAuthority('ROLE_MED')))")
public class AdminController {
	
	@GetMapping("/welcome")
	public @ResponseBody String printAdminPage() {
		return "this is admin page!";
	}
}
